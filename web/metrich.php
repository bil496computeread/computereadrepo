<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<title>ComputeREAD</title>
	<style>@import url('https://fonts.googleapis.com/css?family=Grand+Hotel');
	body{
		background-color: #48022B;
		}a{color: #fff;
		font-family: 'Grand Hotel' !important;
		font-size:25px;
		}
		</style>
</head>
<body>
	
	<a href="menu.php">Ana Sayfa</a>
	
	<div class="list-group">
  
  <a href="rapunzelmetric.php" class="list-group-item list-group-item-action list-group-item-white">Rapunzel</a>
  <a href="satrancmetric.php" class="list-group-item list-group-item-action list-group-item-secondary">Satranç</a>
  <a href="kurkmantolumadonnametric.php" class="list-group-item list-group-item-action list-group-item-white">Kürk Mantolu Madonna</a>
  <a href="kasagimetric.php" class="list-group-item list-group-item-action list-group-item-secondary">Kaşağı</a>
  <a href="kulkedisimetric.php" class="list-group-item list-group-item-action list-group-item-white">Külkedisi</a>
  <a href="kucukprensmetric.php" class="list-group-item list-group-item-action list-group-item-secondary">KüçükPrens</a>
   <a href="guzelmetric.php" class="list-group-item list-group-item-action list-group-item-white">Güzel Ve Çirkin</a>
  <a href="hanselmetric.php" class="list-group-item list-group-item-action list-group-item-secondary">Hansel Ve Gratel</a>
  <a href="agrimetric.php" class="list-group-item list-group-item-action list-group-item-white">Ağrı Dağı Efsanesi</a>
</div>
	
	
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>