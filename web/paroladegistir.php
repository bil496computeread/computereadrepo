<?php
	require_once "oturumkontrolu.php";

	if ( isset($_POST["pass1"]) ) {
		if( $_POST["pass2"] != $_POST["pass3"] ) {
			echo "Yeni Parola aynı değil...";
			die();
		}	
	}

	if ( isset($_POST["pass1"]) ) {
		// Form submit edildi
		// echo "<pre>"; print_r($_POST); echo "</pre>";
		require_once "config.php";

		$SQL = sprintf("UPDATE kullanicilar SET 
						kullaniciparolasi  = '%s'
						WHERE id = '%s' AND kullaniciparolasi = '%s' ", 
					   $_POST["pass2"], $_SESSION["id"], $_POST["pass1"] );
		$result = mysqli_query($mysqli, $SQL);

		header("Location: islemtamam.php");
		die();
	}


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
	<title>ComputeRead</title>
	<style>body{
		background-color: #48022B;
		}</style>
		<style>h1{
	color:#fff;
	}
	a{ color: #fff;
	}</style>
</head>
<body>

	<a href="menu.php">Ana Sayfa</a>


	<h1>Parolanızı Güncelleyin...</h1>
	

	
	<div class="container">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="#" role="login">
          <img src="https://cdn0.iconfinder.com/data/icons/learning-icons-1/110/Owl-Book-256.png" class="img-responsive" alt="" />
		 
          <input type="password" name="pass1" placeholder="Mevcut Şifreniz" class="form-control input-lg" value=""  />
		   <input type="password" name="pass2"  placeholder="Yeni Şifreniz" class=" form-control input-lg" value=""  />
		    <input type="password" name="pass3"  placeholder="Yeni Şifreniz Tekrar" class="form-control input-lg" value=""  />
          
          <div class="pwstrength_viewport_progress"></div>
          
          
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Güncelle</button>
      
          
        </form>
        
      </section>  
      </div>
      
      <div class="col-md-4"></div>
      

  </div>
	
	
	

   <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>