import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class GoruntuIsleme {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();

        Process proc = null;
        System.out.println("Fotoğrafın adını uzantısı ile birlikte (.jpg/.png/.jpeg )veriniz: ");
        Scanner k = new Scanner(System.in);
        String kitap = k.nextLine();
        String command = "tesseract " + kitap + " okunan"
        try {
            proc = rt.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()))) {

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));
            //read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            String s = null;
            while ((s = stdInput.readLine()) != null) System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}