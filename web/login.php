<?php

	session_start();
	if( isset( $_SESSION["girisyapti"] ) and  $_SESSION["girisyapti"] == 1) {
		// Zaten login olmuş. Menüye yönlendirelim...
		header("Location: menu.php");
		die();
	}


	if ( isset($_POST["user"]) ) { // Login olmaya çalışıyoruz
		$user = $_POST["user"];
		$pass = $_POST["pass"];

		require_once "config.php";

		$SQL = "select * from kullanicilar 
			where kullaniciadi = '{$user}' AND kullaniciparolasi = '{$pass}' ";

		$kayitlar = mysqli_query($mysqli, $SQL);

		$kayit = mysqli_fetch_array($kayitlar);

		$KayitAdedi = mysqli_num_rows ($kayitlar);


		if( $KayitAdedi == 1 ) { // login başarılı :)
			session_start();
			$_SESSION["adisoyadi"]  = $kayit["adisoyadi"];
			$_SESSION["id"]         = $kayit["id"];
			$_SESSION["girisyapti"] = 1;
			header("Location: menu.php");
		} else {
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
			$_SESSION["girisyapti"]   = 0;
			echo "parola yanlış";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">

	<title>ComputeREAD</title>
<style>body{
		background-color: #48022B;
		}</style>
</head>
<body>
<a href="../index.html"> Ana Sayfa</a>

<div class="container">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="#" role="login">
          <img src="https://cdn0.iconfinder.com/data/icons/learning-icons-1/110/Owl-Book-256.png" class="img-responsive" alt="" />
          <input type="text" name="user" placeholder="Kullanıcı Adı" required class="form-control input-lg" value="" autocomplete="off" />
          <input type="password" name = "pass" class="form-control input-lg" id="password" placeholder="Kullanıcı Parolanız" required="" value="" />
          
          <div class="pwstrength_viewport_progress"></div>
          
          
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Giriş Yap</button>
      
          
        </form>
        
      </section>  
      </div>
      
      <div class="col-md-4"></div>
      

  </div>

<script>
	function registerOl2Clicked () {
		window.location = '/register2.php'
	}
	
</script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>