package com.example.user.ersteandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
	
	private TextView mDisplayQuestion;
    private TextView mDisplayInfo;
    private TextView mDisplayPoints;
    private Button mBtnAnswer1;
    private Button mBtnAnswer2;
    private Button mBtnAnswer3;
    private Button mBtnAnswer4;

    private int questionCount = 0;

    private int mTotalPoints = 0;
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_layout);
    }

    public void metric(View v)
    {
		setContentView(R.layout.metric);
		 
        int h1 = 0;
        int h2 = 0;
        int h3 = 0;
        int h4 = 0;
        int h5 = 0;
        int h6plus = 0;
        int toplamkelime = 0;
		
		String text = "";
        byte buff[];
		
		try
		{
			InputStream is = getAssets().open("okunan.txt");
            int size = is.available();
            buff = new byte[size];
            is.read(buff);
            is.close();
            text = new String(buff);
			
			String secilecekharfler = "aeıioöuü";
			int i = 0;
			int a;
			int b;
			Reader r = new StringReader(text);
            BufferedReader br = new BufferedReader(r);
			String satir;
			satir = br.readLine();
			
			// HECE HESAPLAMA
			while (satir != null) { //Satır okuma
			
				//System.out.println(satir);
				String[] kelimeler = satir.split(" ");
				int count = kelimeler.length;
				//if (count > 1)
				// System.out.println("Kelime Sayısı = " + count);

				for (a = 0; a < count; a++) { // arrayden kelimeleri alıp inceleme

					for (b = 0; b < kelimeler[a].length(); b++) { // hece hedsplama
						for (int c = 0; c < secilecekharfler.length(); c++) {
							if (kelimeler[a].charAt(b) == secilecekharfler.charAt(c))
								i++;
						}//3. FOR

					}//2.FOR

					if (i == 1) h1++;
					if (i == 2) h2++;
					if (i == 3) h3++;
					if (i == 4) h4++;
					if (i == 5) h5++;
					if (i >= 6) h6plus++;
					// System.out.println((a + 1) + ". kelime :" + kelimeler[a] + "  " + i + "heceli");
					i = 0;

				}//1.FOR
				toplamkelime = h1 + h2 + h3 + h4 + h5 + h6plus;
				//System.out.println("h1=" + h1 + " h2=" + h2 + " h3=" + h3 + ", h4=" + h4 + " h5=" + h5 + " h6plus=" + h6plus + " toplamkelime=" + toplamkelime);
				satir = br.readLine();
			}// While ın sonu

			// CÜMLE SAYISI HESAPLAMA
			Reader r2 = new StringReader(text);
            BufferedReader br2 = new BufferedReader(r2);
			String cs;
			int d;
			int h;
			int cumlesayisi = 0;
			String secilecekisaretler = ".?!";
			cs = br2.readLine();
			
			while (cs != null) {
				for (d = 0; d < cs.length(); d++) {
					for (h = 0; h < secilecekisaretler.length(); h++) {
						if (cs.charAt(d) == secilecekisaretler.charAt(h))
							cumlesayisi++;
					}//3. FOR
				}

				cs = br2.readLine();

			}
			//System.out.println("cümle sayisi = " + cumlesayisi);
			double oks = toplamkelime / cumlesayisi;
			double h3k = (double) h3 / cumlesayisi;
			double h4k = (double) h4 / cumlesayisi;
			double h5k = (double) h5 / cumlesayisi;
			double h6pk = (double) h6plus / cumlesayisi;
			//System.out.println(oks);
			double sn = Math.sqrt(oks * ((h3k * 0.84) + (h4k * 1.5) + (h5k * 3.5) + (h6pk * 26.25)));
			
			TextView textElement =(TextView) findViewById(R.id.textView);
            String str = sn + "";
            textElement.setText(str);

            TextView sonuc =(TextView) findViewById(R.id.sonuc);
            //String st = "h1=" + h1 + " h2=" + h2 + " h3=" + h3 + " h4=" + h4 + " h5=" + h5 + " h6plus=" + h6plus + " toplamkelime=" + toplamkelime + "";
            sonuc.setText(st);

            sonuc.setMovementMethod(new ScrollingMovementMethod());
            textElement.setMovementMethod(new ScrollingMovementMethod());

            TextView level = (TextView) findViewById(R.id.level);
            String levelHeader = "BASLANGIC < ORTA < UST DUZEY < ILERI DUZEY";

            String levelContent = "";

            if (sn < 9)
                levelContent = "Kitabın seviyesi: BASLANGIC";
            if (9 <= sn && sn <= 12)
                levelContent = "Kitabın seviyesi: ORTA";
            if (12 <= sn && sn <= 16)
                levelContent = "Kitabın seviyesi: UST DUZEY";
            if (sn > 16)
                levelContent = "Kitabın seviyesi: ILERI DUZEY";

            level.setText(levelHeader + "\n" + levelContent);
		}
		
		catch(IOException e)
        {
            e.printStackTrace();
        }

    }
   public void test(View v)
    {

        setContentView(R.layout.test);

        mDisplayQuestion = (TextView) findViewById(R.id.tv_question_display);

        mDisplayInfo = (TextView) findViewById(R.id.tv_answer_info);

        mDisplayPoints = (TextView) findViewById(R.id.tv_show_points);


        mBtnAnswer1 = (Button) findViewById(R.id.btn_answer1);
        mBtnAnswer2 = (Button) findViewById(R.id.btn_answer2);
        mBtnAnswer3 = (Button) findViewById(R.id.btn_answer3);
        mBtnAnswer4 = (Button) findViewById(R.id.btn_answer4);


        prepareQuestion(questionCount);
        puanHesaplama(v);



   /*     mBtnAnswer1.setOnClickListener();
        mBtnAnswer2.setOnClickListener(this);
        mBtnAnswer3.setOnClickListener(this);
        mBtnAnswer4.setOnClickListener(this);*/


    }
    public void prepareQuestion(int questionCount){

        String[][] questions = questionsAnswers.getQuestion();


        if(questionCount < questions.length){

            String[] choices = questions[questionCount][1].split(",");

            System.out.println("1**********"+ questionCount);

            mDisplayQuestion.setText(questions[questionCount][0]);

            System.out.println("2***********"+ questionCount);

            mBtnAnswer1.setText(choices[0]);
            mBtnAnswer2.setText(choices[1]);
            mBtnAnswer3.setText(choices[2]);
            mBtnAnswer4.setText(choices[3]);
        }else {

            mBtnAnswer1.setVisibility(View.INVISIBLE);
            mBtnAnswer2.setVisibility(View.INVISIBLE);
            mBtnAnswer3.setVisibility(View.INVISIBLE);
            mBtnAnswer4.setVisibility(View.INVISIBLE);
            mDisplayQuestion.setText("Testi bitirdiniz.");
        }

    }
    public void puanHesaplama(View view){
        Button b = (Button)view;
        String buttonText = b.getText().toString();
        String[] answer = questionsAnswers.getAnswer();


        if(Objects.equals(buttonText, answer[questionCount]) && !buttonText.equals("Test Yap")){
            mDisplayInfo.setText("Doğru cevap !");
            mTotalPoints = mTotalPoints + 3 ;

        }else if(!buttonText.equals("Test Yap")) {
            mDisplayInfo.setText("Yanlış cevap !");
            mTotalPoints = mTotalPoints - 1;
        }
        questionCount++;
        prepareQuestion(questionCount);
        mDisplayPoints.setText("Toplam puanınız:  " + String.valueOf(mTotalPoints));
    }

}

	
	
	}